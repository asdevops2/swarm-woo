FROM wordpress:latest

ARG UNAME=www-data
ARG UGROUP=www-data
ARG UID=1000
ARG GID=1000

RUN usermod -u $UID $UNAME && groupmod -g $GID $UGROUP