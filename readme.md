# DEMO PROJECT to deploy a Woocommerce with ansible and docker swarm on VMs

## 1- CREATE AND CONFIG VMs
    - create vagrant file with 3 virtuals machines : 1 manager and 2 workers
    - cmd: vagrant up (to launch virtual machine)
    - init git project

## 2- CONFIGURE Ansible tool
    - create a ansible.cfg for global config
    - create an inventory file to fix and manage hosts
    - create a playbook to install docker and dependecies 
        cmd: ansible-playbook ./ansible/install-docker.yml -i ./vagrant/inventory.ini

## 3- CONFIGURE DOCKER SWARM
    - create a playbook to configure docker swarm
        cmd: ansible-playbook ./ansible/config-swarm.yml -i ./vagrant/inventory.ini
        ## NB: On local virtual machine to Define manager ipv4.address -> check with vagrant ssh-config, store the ip define in inventory.ini by check ansible_all_ipv4_adresses

## 4- INSTALL WORDPRESS LATEST
    - create docker compose with wordpress:latest, mariadb:latest, wpcli, and nginx:latest
    - create a nginx.conf file to manage host
    - create a ansible playbook to install on global swarm stack
    - deploy stack on local virtual machines
        cmd: ansible-playbook ./ansible/install-wordpress.yml -i ./vagrant/inventory.ini
    - wait for wpcli préconfigure wordpress with this command
        'wp core install
        --path=/var/www/html
        --url=localhost:8000
        --title="wordpress1"
        --admin_user=admin
        --admin_password=admin
        --admin_email=admin@email.fr
        --skip-email'

## 5- USE A DOCKERFILE TO BUILD AN WORDPRESS services
    - create dockerfile form wordpress latest and configure 